'use strict';
// --------------------------------*--------------------------------
// -------------------------- Constraints --------------------------
const peerConnectionConfiguration = {
    iceServers: [
        {
            urls: [
                'stun:stun1.l.google.com:19302',
                'stun:stun2.l.google.com:19302',
            ],
        },
    ],
    iceCandidatePoolSize: 10,
};
window.addEventListener('DOMContentLoaded', async () => {
    window.addEventListener('message', e => {
        if (!e.origin.includes("tribeplatform.com"))
            return;

        switch (e.data.type) {
            case 'call':
                initDomListeners(e.data.targetUsername);
                document.querySelector("#display_name").innerHTML = e.data.calleeDisplayName;
                document.querySelector("#status_string").innerHTML = "Ringing...";
                document.querySelector("#hangupButton").classList.replace("display-none", "display-flex");
                document.querySelector("#answerButton").classList.replace("display-flex", "display-none");
                document.querySelector("#rejectButton").classList.replace("display-flex", "display-none");
                if (e.data.calleeAvatarSrc) {
                    document.querySelector("#avatar_img").src = e.data.calleeAvatarSrc;
                    document.querySelector("#avatar_img").classList.replace('display-none', 'display-block');
                    document.querySelector("#avatar_svg").classList.replace('display-block', 'display-none');
                } else {
                    document.querySelector("#avatar_svg").classList.replace('display-none', 'display-block');
                    document.querySelector("#avatar_img").classList.replace('display-block', 'display-none');
                }
                document.querySelector("#ring").play();
                displayCall.classList.replace('display-none', 'display-flex');
                onCall(e.data.targetUsername, e.data.callerDisplayName, e.data.callerAvatarSrc);
                break;
        }
    });
    // ------------------- Extracting Query Params --------------------
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    const activeUsername = params.active;
    // ----------------------- Global variables ------------------------
    const displayCall = document.querySelector('#display-call');
    const db = firebase.firestore();
    let localAudioStream = null;
    let peerConnection = null;

    // ----------------------- Global delegates ------------------------
    let onCall = async (targetUsername, callerDisplayName, callerAvatarSrc) => {
        await prepareMedia();
        await fbRequestCall(targetUsername, callerDisplayName, callerAvatarSrc);
    };
    const onAnswer = async () => {
        window.parent.postMessage({ type: "calling" }, "*");
        document.querySelector("#status_string").innerHTML = "Connecting...";
        document.querySelector("#hangupButton").classList.replace("display-none", "display-flex");
        document.querySelector("#answerButton").classList.replace("display-flex", "display-none");
        document.querySelector("#rejectButton").classList.replace("display-flex", "display-none");
        await prepareMedia();
        await initCall(activeUsername);
    };
    const onReject = async () => {
        fbRejectCall();
        hangup();
        window.parent.postMessage({ type: "hangup" }, "*");
    };
    const onHangup = async (username) => {
        fbHangup(username);
        hangup();
        window.parent.postMessage({ type: "hangup" }, "*");
    };

    // ------------------------ Listen to glob ------------------------
    let activeGlobRef = await db.collection('glob').doc(activeUsername);
    activeGlobRef.onSnapshot(async snapshot => {
        if (snapshot.metadata.hasPendingWrites) return; // This is because of local broadcast
        console.log('Got updated glob document:', snapshot.data());
        // data: {callerUsername, timestamp, callId, status}
        const data = snapshot.data();
        if (!data) return;
        switch (data.status) {
            case 'calling':
                initDomListeners(activeUsername);
                window.parent.postMessage({ type: "callReceived" }, "*");
                document.querySelector("#display_name").innerHTML = data.callerDisplayName;
                document.querySelector("#status_string").innerHTML = "Ringing...";
                document.querySelector("#hangupButton").classList.replace("display-flex", "display-none");
                document.querySelector("#answerButton").classList.replace("display-none", "display-flex");
                document.querySelector("#rejectButton").classList.replace("display-none", "display-flex");
                if (data.callerAvatarSrc) {
                    document.querySelector("#avatar_img").src = data.callerAvatarSrc;
                    document.querySelector("#avatar_img").classList.replace('display-none', 'display-block');
                    document.querySelector("#avatar_svg").classList.replace('display-block', 'display-none');
                } else {
                    document.querySelector("#avatar_svg").classList.replace('display-none', 'display-block');
                    document.querySelector("#avatar_img").classList.replace('display-block', 'display-none');
                }
                document.querySelector("#ring").play();
                displayCall.classList.replace('display-none', 'display-flex');
                break;
            case 'free':
                hangup();
                document.querySelector("#ring").pause();
                document.querySelector("#ring").currentTime = 0;
                window.parent.postMessage({ type: "hangup" }, "*");
                break;
        }
    });

    // ------------------------ FB Requests ------------------------
    const fbRequestCall = async (targetUsername, callerDisplayName, callerAvatarSrc) => {
        let passiveGlobRef = await db.collection('glob').doc(targetUsername);
        let latestGlob = await passiveGlobRef.get();
        if (!latestGlob.exists) await passiveGlobRef.set({ status: 'free' });
        else if (latestGlob.data()?.status == 'calling') return console.log(`${targetUsername} is busy now!`);
        let callRef = await db.collection('calls').doc();
        callRef.set({ status: 'justCreated', timestamp: firebase.firestore.FieldValue.serverTimestamp() });
        await passiveGlobRef.set({ callerUsername: activeUsername, callerDisplayName, callerAvatarSrc, timestamp: firebase.firestore.FieldValue.serverTimestamp(), callId: callRef.id, status: 'calling' });
        callRef.onSnapshot(async snapshot => {
            if (snapshot.metadata.hasPendingWrites) return; // This is because of local broadcast
            console.log('Got updated call document:', snapshot.data());
            // data: {timestamp, status}
            const data = snapshot.data();
            if (!data) return;
            switch (data.status) {
                case 'justCreated':
                    break;
                case 'offer':
                    handleOffer(targetUsername, data.offer);
                    break;
                case 'rejected':
                    hangup();
                    document.querySelector("#ring").pause();
                    document.querySelector("#ring").currentTime = 0;
                    window.parent.postMessage({ type: "reject" }, "*");
                    break;
                case 'hangedup':
                    hangup();
                    document.querySelector("#ring").pause();
                    document.querySelector("#ring").currentTime = 0;
                    window.parent.postMessage({ type: "hangup" }, "*");
                    break;
            }
        });
    };

    const fbRejectCall = async () => {
        let globRef = await db.collection('glob').doc(activeUsername);
        let latestGlob = await globRef.get();
        if (!latestGlob.exists) return console.log(`No call exist!`);
        else if (latestGlob.data()?.status === 'free') return console.log(`No call exist!`);
        if (latestGlob.data()?.callId) {
            await db.collection('calls').doc(latestGlob.data().callId).update({ status: 'rejected' });
        }
        await globRef.update({ status: "free" });
    };

    const fbHangup = async (targetUsername) => {
        let globRef = await db.collection('glob').doc(targetUsername);
        let latestGlob = await globRef.get();
        if (!latestGlob.exists) return console.log(`No call exist!`);
        else if (latestGlob.data()?.status === 'free') return console.log(`No call exist!`);
        if (latestGlob.data()?.callId) {
            await db.collection('calls').doc(latestGlob.data().callId).update({ status: 'hangedup' });
        }
        await globRef.update({ status: "free" });
    };

    const fbOffer = async (callRef, offer) => {
        await callRef.update({ status: 'offer', offer: { type: 'offer', sdp: offer.sdp } });
    }

    const fbAnswer = async (callRef, answer) => {
        await callRef.update({ status: 'answer', answer: { type: 'answer', sdp: answer.sdp } });
    }

    const prepareMedia = async () => {
        localAudioStream = await navigator.mediaDevices.getUserMedia({ audio: true, video: false });
    }

    const initCall = async (username) => {
        let passiveGlobRef = await db.collection('glob').doc(username);
        let latestGlob = await passiveGlobRef.get();
        let callRef = await db.collection('calls').doc(latestGlob.data()?.callId);
        await createPeerConnection(callRef, false);
        const offer = await peerConnection.createOffer();
        await peerConnection.setLocalDescription(offer);
        await fbOffer(callRef, offer);
        callRef.onSnapshot(async snapshot => {
            if (snapshot.metadata.hasPendingWrites) return; // This is because of local broadcast
            console.log('Got updated call document:', snapshot.data());
            // data: {timestamp, status}
            const data = snapshot.data();
            if (!data) return;
            switch (data.status) {
                case 'answer':
                    handleAnswer(data.answer);
                    break;
            }
        });
    }

    const createPeerConnection = async (callRef, amICaller) => {
        console.log("createPeerConnection", amICaller);
        peerConnection = new RTCPeerConnection(peerConnectionConfiguration);
        peerConnection.onicecandidate = event => {
            const message = {
                type: 'candidate',
                candidate: null,
            };
            if (event.candidate) {
                message.candidate = event.candidate.candidate;
                message.sdpMid = event.candidate.sdpMid;
                message.sdpMLineIndex = event.candidate.sdpMLineIndex;
            }

            callRef.collection(amICaller ? "callee" : "caller").onSnapshot(async snapshot => {
                snapshot.docChanges().forEach(change => {
                    if (change.type === "added") {
                        handleCandidate(change.doc.data())
                    }
                })
            });
            callRef.collection(amICaller ? "caller" : "callee").add(message);
        };

        peerConnection.ontrack = event => audioElement.srcObject = event.streams[0];
        localAudioStream.getTracks().forEach(track => peerConnection.addTrack(track, localAudioStream));
    }

    const handleCandidate = async (candidate) => {
        if (!peerConnection) return;
        if (!candidate.candidate) {
            await peerConnection.addIceCandidate(null);
        } else {
            await peerConnection.addIceCandidate(candidate);
        }
    }

    const handleOffer = async (targetUsername, offer) => {
        if (peerConnection) return;
        let passiveGlobRef = await db.collection('glob').doc(targetUsername);
        let latestGlob = await passiveGlobRef.get();
        let callRef = await db.collection('calls').doc(latestGlob.data()?.callId);
        await createPeerConnection(callRef, true);
        await peerConnection.setRemoteDescription(offer);
        const answer = await peerConnection.createAnswer();
        await peerConnection.setLocalDescription(answer);
        await fbAnswer(callRef, answer);
        document.querySelector("#status_string").innerHTML = "Calling...";
        document.querySelector("#ring").pause();
        document.querySelector("#ring").currentTime = 0;
    }

    const handleAnswer = async (answer) => {
        if (!peerConnection) return;
        await peerConnection.setRemoteDescription(answer);
        document.querySelector("#status_string").innerHTML = "Calling...";
        document.querySelector("#ring").pause();
        document.querySelector("#ring").currentTime = 0;
    }

    const hangup = async () => {
        if (peerConnection) {
            peerConnection.close();
            peerConnection = null;
        }
        if (localAudioStream) localAudioStream.getTracks().forEach(track => track.stop());
        localAudioStream = null;
        document.querySelector("#ring").pause();
        document.querySelector("#ring").currentTime = 0;
    };

    // ------------------------- Dom Section -------------------------
    const answerButton = document.getElementById('answerButton');
    const rejectButton = document.getElementById('rejectButton');
    const hangupButton = document.getElementById('hangupButton');

    const audioElement = document.getElementById('audio');

    const initDomListeners = (username) => {
        hangupButton.onclick = () => onHangup(username);
    }

    answerButton.onclick = () => onAnswer();
    rejectButton.onclick = onReject;
});