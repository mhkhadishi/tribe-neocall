window.addEventListener('DOMContentLoaded', function () {
    const neoReady = new CustomEvent('neoReady', {
        detail: {}
    });

    window.addEventListener('tribeCred', (e) => {
        let activeUsername = e.detail.username;
        let teammate = e.detail.teammate;
        let activeName = e.detail.name;
        let activeAvatarSrc = e.detail.profilePictureId ? `https://tribe-s3-production.imgix.net/${e.detail.profilePictureId}?w=500&auto=compress,format&dl` : null;

        function getEncodedLink() {
            var data = '{{json}}';
            data = btoa(encodeURIComponent(data));
            return `https://tribe-neocall.firebaseapp.com/index.html?active=${activeUsername}`;
            // return `http://localhost:5000/index.html?active=${activeUsername}`;
        }

        let neoIframe = null;

        function initIframe(iframe) {
            neoIframe = document.querySelector("#neo_call_iframe");
            if (neoIframe) return;

            neoIframe = document.createElement('iframe');
            neoIframe.id = 'neo_call_iframe';
            neoIframe.style.cssText = 'position:fixed !important; bottom: 30px !important; right: 30px !important; z-index: 3999; width:400px; height: 75px;';
            neoIframe.style.display = 'none';
            neoIframe.src = getEncodedLink();
            neoIframe.allow = "autoplay; camera; microphone";
            document.body.appendChild(neoIframe);
        }

        initIframe();

        window.addEventListener('message', e => {
            // if (e.origin !== "http://localhost:5000")
            //     return;

            switch (e.data.type) {
                case 'hangup':
                case 'reject':
                case 'callReceived':
                case 'calling':
                    composeIFrame(e.data);
                    break;
            }
        }, false);

        function composeIFrame(data) {
            switch (data.type) {
                case 'reject':
                case 'hangup':
                    neoIframe.style.display = 'none';
                    const callButtonElement = document.querySelector('#callButton');
                    if (callButtonElement) callButtonElement.disabled = false;
                    break;
                case 'callReceived':
                    neoIframe.style.display = 'block';
                    break;
                case 'callStarted':
                    neoIframe.style.display = 'block';
                    break;
                case 'calling':
                    neoIframe.style.display = 'block';
                    break;
            }
        }

        // ---------------------------- Member page logic ----------------------------
        const handlePageView = (e) => {
            if (window.location.pathname.startsWith('/member/')) {
                const splits = window.location.pathname.replace('/member/', '').split('/');
                if (splits.length == 0) return;
                passiveUsername = splits[0];
                if (activeUsername == passiveUsername) return;
                setTimeout(function () {
                    const bannerElement = document.querySelectorAll('main img')[0];
                    const buttonElement = createCallButtonDoms(bannerElement);
                    buttonElement.onclick = (e) => {
                        e.stopPropagation();
                        let sourcesOfData = document.querySelectorAll("img.rounded-full");
                        let sourceOfData = { alt: "Member: " + passiveUsername, src: null }
                        if (sourcesOfData.length > 1) {
                            sourceOfData = sourcesOfData[1];
                        } else if (sourcesOfData[0].alt != activeName) {
                            sourceOfData = sourcesOfData[0];
                        }

                        neoIframe.contentWindow.postMessage({ type: "call", targetUsername: passiveUsername, callerDisplayName: activeName, callerAvatarSrc: activeAvatarSrc, calleeDisplayName: sourceOfData.alt, calleeAvatarSrc: sourceOfData.src }, "*");
                        buttonElement.disabled = true;
                        composeIFrame({ type: "callStarted" });
                    }
                }, 1000);
            }
        }
        const createCallButtonDoms = (bannerElement) => {
            let buttonElement = document.querySelector('#callButton');
            if (buttonElement) return buttonElement;
            buttonElement = document.createElement('button');
            buttonElement.type = 'button';
            buttonElement.id = 'callButton';
            buttonElement.style = ` width: 35px;
                                    height: 35px;
                                    background-color: rgb(255 255 255);
                                    border-radius: 100%;
                                    margin: 5px;
                                    display: flex;
                                    position: absolute; top: 10px; right: 10px;
                                    justify-content: center;
                                    align-items: center;
                                    z-index: 3999`
            const embedElement = document.createElement('img');
            embedElement.style = 'pointer-events: none';
            embedElement.src = 'https://tribe-neocall.firebaseapp.com/assets/svgs/accept-call-svgrepo-com.svg';
            buttonElement.appendChild(embedElement);
            if (teammate) bannerElement.parentNode.parentNode.parentNode.prepend(buttonElement);
            else bannerElement.parentNode.prepend(buttonElement);

            return buttonElement;
        }
        window.addEventListener('pageView', (e) => {
            handlePageView(e);
        });
        handlePageView();
    });
    window.dispatchEvent(neoReady);

}, false);