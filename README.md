# tribe-neocall

The Tribe-NeoCall project intends to add an end to end encrypted call over internet feature to the Tribe Platform. This project is only for assignment purposes.

By installing NeoCall app on a Tribe community, members will be capable to call each other like as users are doing in Whatsapp!

NeoCall is using WebRTC technology for P2P calling and also using Firestore for real-time signaling.

**Note:** NeoCall is on prototype staging to show how Tribe can have a call feature and it doesn't consider the security and privilage concerns for now. The call initiation should be done in the backend and firestore records should be managed to be accissible only for call siders.

**Please make sure you allow the microphone permissoin when you want to start a call.** 
## Getting started
**Note:** For quick start, you can only follow the step 6 and using the test firestore.

1. Create a firebase project.
2. Clone the master branch of NeoCall.
3. Replace the firebase connection details that are located in the bottom of index.html file with your own firebase project.
4. Deploy the project on Firebase Hosting by using "firebase deploy" command.
5. Replace your Firebase hostname with existing one.
6. Create a new Application inside your Tribe community, and enter the following custom codes in your application:

###### Custom Code for Header:
```html
<meta http-equiv="Content-Security-Policy" content="img-src 'self' https: data:">
```
###### Custom Code for Body:
```html
<script src="https://tribe-neocall.firebaseapp.com/js/release.js" type="text/javascript"></script>
<script>
    window.addEventListener('neoReady', () => {
        const tribe = new CustomEvent('tribeCred', {
            detail: {
                username: '{{ member.id }}',
                teammate: {{ member.teammate }},
            }
        });
        window.dispatchEvent(tribe);
    });
    setTimeout(() => {
        
        window.Tribe.on('pageView', (previousPath, newPath) => {
            const pageView = new CustomEvent('pageView', {
                detail: {}
            });
            window.dispatchEvent(pageView);
        })
    }, 1000);
</script>
```

7. Install your NeoCall on your Tribe community.
8. Login to your Tribe community account.
9. Go to someone's profile and press the call button that is located in the banner section.